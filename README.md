#Challange chapter 5 <br>

##ERD<br>

![msg-1798812755-1716](/uploads/ff2f877f7520ddc91aa11b7f6054835e/msg-1798812755-1716.jpg)

#link<br>
// Render view <br>
app.get('/cars', carsController.getAllCars);<br>
app.get('/cars/add', carsController.renderCreateCarForm);<br>
app.get('/cars/update/:id', carsController.renderUpdateCarForm);<br>

// Endpoint logic<br>
app.post('/cars', carsController.createNewCar);<br>
app.post('/cars/:id', carsController.updateCar);<br>
app.get('/cars/:id', carsController.deleteCar);<br>


const path = require('path');
const express = require('express');
const app = express(); // Initialize server express
const PORT = 8001; // Listen port
const PATH_DIR = __dirname + '/../public/';

app.use(express.static('public'));
app.get('/', (req, res) => {
    res.sendFile(path.join(PATH_DIR + 'index.html'));
});
app.get('/cari-mobil', (req, res) => {
    res.sendFile(path.join(PATH_DIR + 'cari-mobil.html'));
});

app.get('/cars', (req, res) => {
    res.sendFile(path.join(PATH_DIR + 'cars.html'));
});
app.listen(PORT, () => console.log(`Server running at localhost:${PORT}`));
